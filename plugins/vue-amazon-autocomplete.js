import Vue from 'vue'

//const VueAmazonAutocomplete = require('amazon-autocomplete/src/amazon-autocomplete');
const VueAmazonAutocomplete = require('~/util/amazon-autocomplete');

Vue.use(VueAmazonAutocomplete)
