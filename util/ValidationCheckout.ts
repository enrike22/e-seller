
export class ValidationCheckout {

    isModelCheckout(actual_address:any,authUser:any,productDetail:any,
                    unit_amount:any,cantidad_model:any,shippingPayTotal:any,
                    totalPrice:any,totalToPay:any,
                    shippingDate:any,paymentSelect:any,alert_select:any){

        let result = {
            isError:false,
            message:"",
        }

        if (!actual_address || !actual_address.id){
            result.isError = true;
            result.message = "Debes ingresar o guardar una direccion";
            return result
        }

        if (!authUser || !authUser.uid || !authUser.displayName || !authUser.email){
            result.isError = true;
            result.message = "Debes registrarte o iniciar sesión";
            return result
        }

        if (!productDetail || !productDetail.id || !productDetail.short_name ||
            !productDetail.image || !productDetail.short_name){
            result.isError = true;
            result.message = "Faltan datos del producto";
            return result
        }

        if (!unit_amount || Number(unit_amount) <= 0){
            result.isError = true;
            result.message = "Falta el precio en unidad";
            return result
        }

        if (!cantidad_model || parseInt(cantidad_model)<1){
            result.isError = true;
            result.message = "Seleciona la cantidad de productos";
            return result
        }

        if (!shippingPayTotal && (shippingPayTotal == null || shippingPayTotal == undefined) ){
            result.isError = true;
            result.message = "No está disponible el envió a tu dirección";
            return result
        }

        if(!totalPrice || Number(totalPrice)<=0){
            result.isError = true;
            result.message = "No hay un monto de pago";
            return result
        }

        if(!totalToPay || Number(totalToPay)<=0){
            result.isError = true;
            result.message = "No hay un monto total de pago";
            return result
        }

        if(!shippingDate || shippingDate == "...por calcular"){
            result.isError = true;
            result.message = "No hay una fecha estimada de envio";
            return result
        }

        if(!paymentSelect || !paymentSelect.id || !paymentSelect.title || !paymentSelect.code){
            result.isError = true;
            result.message = "Debes seleccionar un método de pago";
            return result
        }

        if(alert_select && productDetail.options && productDetail.options.length>0){
            result.isError = true;
            result.message = "Debes seleccionar el tipo de pedido";
            return result
        }

        return result
    }

}
