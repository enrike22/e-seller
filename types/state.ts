export interface RootState {
    token:  string | null,
    userInfo: any,
    isLogged : any,
    isLoadFullScrees: any
    userName: null,
    userEmail: null,
    userAvatar: null,

    /* NavBar */
    isNavBarVisible: true,

    /* FooterBar */
    isFooterBarVisible: true,

    /* Aside */
    isAsideVisible: true,
    isAsideMobileExpanded: false,

    /* Dark mode */
    isDarkModeActive: false,

    authUser: any,

    isLoginDialogShow: any,

    searchInput:any,

    detailProduct:any,

}
