import axios from "axios"

export let PARAMETERS:any =  {
    loginSession: 'userSession',
    trackingCampaign: 'trackingCampaign',
    urlModalSession: 'urlModal',
    sessionUrl: '.ofertaup.com',
    url: 'http://ofertaup.pe',
    baseUrlDev: 'https://service.ofertaup.com',
    baseUrl: 'https://service.ofertaup.com',
    baseFirestore:"",
};
export let FIREBASE :any = {
    baseFirestore:""
}

export const HTTP = axios.create({
    baseURL: PARAMETERS.baseUrl
});

export const HTTPDEV = axios.create({
    baseURL: PARAMETERS.baseUrlDev,
});

