import { Middleware } from '@nuxt/types'

const Middleware: Middleware = async ({ app, store, redirect, route  }) => {
  if (app.$fire.auth) {
    //console.log(store.state.authUser.providerData)
    // INFO -> Firebase Services can be accessed with app.$fire.auth (etc.) in Middleware.
  }

  if (route.name != 'validar-gmail' && store.state.authUser && store.state.authUser.providerData){
    //console.log(store.state.authUser)
    let providerData = store.state.authUser.providerData
    let fetchPassword:any = providerData.find(vl=>vl.providerId==="password")
    let fetchGoogle:any = providerData.find(vl=>vl.providerId==="google.com")
    if (fetchGoogle && !fetchPassword){
      redirect('/validar-gmail')
    }
  }

  if (route.name === 'logout' && store.state.authUser) {
    await app.$fire.auth.signOut()
    let authUser = null
    let claims = null
    await store.dispatch('onAuthStateChanged',{ authUser,claims })
    if (!store.state.authUser) {
      //console.log(route)
      return redirect('/')
    }
  }

  if (route.name === 'logout' && !store.state.authUser) {
    return redirect('/');
  }

  if (route.name === 'checkout' && !store.state.authUser) {
    if(route.query.objectId&&route.query.orderFrom&&route.query.quantity){
      return redirect({path:'/ingresar', query:{
          toPath:route.name,
          objectId:route.query.objectId,
          orderFrom:route.query.orderFrom,
          quantity:route.query.quantity
        }});
    }else {
      return redirect('/ingresar')
    }
  }

  if(route.name==='ingresar' && store.state.authUser){
    return redirect('/')
  }

  if(route.name==='validacion' && !store.state.authUser){
    return redirect('/')
  }

  if (route.name === 'checkout' && store.state.authUser && route.query
      && (!route.query.objectId || !route.query.orderFrom)) {
    return redirect('/not-found-page');
  }

  if (route.name === 'checkout-success' && !store.state.authUser) {
    return redirect('/ingresar');
  }

  if (route.name === 'checkout-success' && store.state.authUser && route.query
      && (!route.query.collection_id || !route.query.collection_status || !route.query.status)) {
    return redirect('/');
  }

  if (route.name === 'perfil' && !store.state.authUser) {
    return redirect({path:'/ingresar',query:{from_path:route.path}});
  }

  if (route.name === 'perfil-mis-pedidos' && !store.state.authUser) {
    return redirect({path:'/ingresar',query:{from_path:route.path}});
  }

  if (route.name === 'perfil-mis-direcciones' && !store.state.authUser) {
    return redirect({path:'/ingresar',query:{from_path:route.path}});
  }

  if (route.name === 'perfil-mis-ventas' && !store.state.authUser) {
    return redirect({path:'/ingresar',query:{from_path:route.path}});
  }

  if (route.name === 'perfil-mis-ventas' && store.state.authUser && !store.state.authUser.roles_id.find(vl=>vl.name=='seller')) {
    return redirect({path:'/'});
  }
}

export default Middleware
