import { RootState } from "~/types/state";
import { MutationTree, ActionTree } from "vuex";
import {FIREBASE,PARAMETERS} from "~/config/HttpBase";

export const state = (): RootState => ({
    token:  null,
    userInfo: null,
    isLogged : false,
    isLoadFullScrees: false,
    /* User */
    userName: null,
    userEmail: null,
    userAvatar: null,

    /* NavBar */
    isNavBarVisible: true,

    /* FooterBar */
    isFooterBarVisible: true,

    /* Aside */
    isAsideVisible: true,
    isAsideMobileExpanded: false,

    /* Dark mode */
    isDarkModeActive: false,

    authUser: null,

    isLoginDialogShow:false,

    searchInput:null,

    detailProduct:null
});

export const mutations: MutationTree<RootState> = {

    setDetailProduct (state,detail) {
        state.detailProduct = detail;
    },

    logout (state) {
        state.token = null;
        state.userInfo = null;
        state.isLogged = false;
    },

    showLoadFullScreen (state) {
        state.isLoadFullScrees = state
    },

    setLoginDialogShow(state, dialog){
        state.isLoginDialogShow = dialog
    },

    setSearchQuery(state, searchtxt) {
        state.searchInput = searchtxt
    },

    RESET_STORE: (state) => {
        state.authUser = null
    },

    SET_AUTH_USER: (state, { authUser,claims }) => {
        state.authUser = {
            uid: authUser.uid,
            email: authUser.email,
            displayName:authUser.displayName,
            phoneNumber:authUser.phoneNumber,
            providerData:authUser.providerData,
            roles_id:authUser.roles_id
        };
    },

    getDetailProduct (state,detail) {
        state.detailProduct = detail;
    },
};

export const getters = {

    getUserAvatar(){
        return ""
    },

    isLoggedIn(state:any) {
        try {
            //console.log(state.authUser);
            //console.info("Login",state.authUser.uid);
            return state.authUser.uid !== null
        } catch {
            return false
        }
    },

    getUserName(){
        return "Samuel"
    },
}

export const actions: ActionTree<RootState, RootState> = {
    nuxtServerInit({ dispatch }, ctx ) {

        // @ts-ignore
        if (this.$fireAuth === null) {
            throw 'nuxtServerInit Example not working - this.$fireAuth cannot be accessed.'
        }

        if (ctx.$fireAuth === null) {
            throw 'nuxtServerInit Example not working - ctx.$fireAuth cannot be accessed.'
        }

        if (ctx.app.$fireAuth === null) {
            throw 'nuxtServerInit Example not working - ctx.$fireAuth cannot be accessed.'
        }

        /** Get the VERIFIED authUser from the server */
        //const cookies = new Cookies(ctx.req.headers.cookie);
        //@ts-ignore
        const getCookiesLogin = this.$cookies.get(PARAMETERS.loginSession)
        if (getCookiesLogin) {
            //@ts-ignore
            this.$cookies.remove(PARAMETERS.loginSession);
            const userCookie = JSON.parse(JSON.stringify(getCookiesLogin));
            let authUser = userCookie.authUser;
            let claims = userCookie.claims;
            dispatch('onAuthStateChanged', {authUser, claims})
        }else {
            if (ctx.res && ctx.res.locals && ctx.res.locals.user) {
                console.log("local cookies")
                const { allClaims: claims, ...authUser } = ctx.res.locals.user;
                dispatch('onAuthStateChanged', {authUser, claims})
            }
        }
        dispatch('onFirestoreApp', {ctx})
    },

    showLoadFullScreen({commit, state}: { commit: any, state: any}, hide: any) {
        commit('showLoadFullScreen', hide);
    },

    onAuthStateChanged({ commit }, { authUser,claims }) {
        if (!authUser && !claims) {
            //@ts-ignore
            this.$cookies.remove(PARAMETERS.loginSession);
            commit('RESET_STORE');
            return
        }

        let CurrentDate = new Date();
        CurrentDate.setMonth(CurrentDate.getMonth() + 2);
        authUser = {
            displayName: authUser.displayName,
            email: authUser.email,
            emailVerified: authUser.emailVerified,
            uid: authUser.uid,
            phoneNumber: authUser.phoneNumber,
            providerData: authUser.providerData,
            photoURL: authUser.photoURL,
            refreshToken: authUser.refreshToken,
            roles_id:authUser.roles_id
        }
        //@ts-ignore
        this.$cookies.set(PARAMETERS.loginSession,{authUser,claims},{
            path: '/',
            expires: CurrentDate,
            sameSite:true
        })
        commit('SET_AUTH_USER', { authUser,claims })
    },

    checkVuexStore(ctx) {
        if (this.$fire.auth === null) {
            throw 'Vuex Store example not working - this.$fire.auth cannot be accessed.'
        }

        alert('Success. Nuxt-fire Objects can be accessed in store actions via this.$fire___')
    },
    onFirestoreApp({ commit }, { ctx }){
        FIREBASE.baseFirestore = ctx.app.$fire.firestore
    }

};
