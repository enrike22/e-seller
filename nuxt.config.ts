import bodyParser from 'body-parser'
//import cookieParser from 'cookie-parser'

export default {
  mode: 'universal',
  head: {
    htmlAttrs: {
      lang: 'es',
      amp: true
    },
    title: 'ofertaup.com',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      {
        vmid: 'description',
        hid: 'description',
        name: 'description',
        content:'¡Tenemos las mejores ofertas de productos tecnológicos, moda, celulares, deportes y más para ti!',
        //content: process.env.npm_package_description || 'Venta de productos importados'
      },
      {
        property: 'keywords',
        content: 'importacion productos electronicos, encuentra los productos mas populares'
      },
      {
        hid:"og:title",
        name:"og:title",
        property:"og:title",
        content:"Compra rápido y seguro con mejores ofertas | Envíos a tu casa | Ofertaup"
      },
      { property: "og:image",
        hid: "og-image",
        vmid: "og-image",
        content: 'https://firebasestorage.googleapis.com/v0/b/e-seller-f54b2.appspot.com/o/generic-images%2Fofertaup-image-home.jpg?alt=media&token=e92cfc8f-1cd7-459d-8c52-9343da29a9ee'
      },
      {
        hid:"og:site_name",
        name:"og:site_name",
        property:"og:site_name",
        content:"ofertaup.com"
      }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      { rel: "apple-touch-icon", sizes: "180x180", href: "/favicon.ico?v=GvmpJqoA5j"},
      { rel: "icon", type: "image/x-icon", sizes: "32x32", href: "/favicon.ico?v=GvmpJqoA5j"},
      { rel: "icon", type: "image/x-icon", sizes: "192x192", href: "/favicon.ico?v=GvmpJqoA5j"},
      { rel: 'dns-prefetch', href: 'https://fonts.gstatic.com' },
      // { rel: 'stylesheet', href: 'https://cdn.jsdelivr.net/npm/instantsearch.css@7.3.1/themes/algolia.css'}
      //{ rel: 'stylesheet', type: 'text/css', href: 'https://fonts.googleapis.com/css?family=Nunito' },
      //{ rel: 'stylesheet', type: 'text/css', href: 'https://cdn.materialdesignicons.com/5.3.45/css/materialdesignicons.min.css' }
    ],
    script: [
      // Font-awesome cdn
      { src: 'https://kit.fontawesome.com/a3533a681e.js' },
      {
        src:
          'https://cdn.jsdelivr.net/npm/instantsearch.js@4.8.3/dist/instantsearch.production.min.js'
      }
      /*{  src: 'https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js', async: true },
      {  src: 'https://securepubads.g.doubleclick.net/tag/js/gpt.js', async: true },
      {
        type: 'text/javascript',
        innerHTML: "(adsbygoogle = window.adsbygoogle || []).push({\n" +
          "    google_ad_client: \"ca-pub-1582418765307341\",\n" +
          "    enable_page_level_ads: true\n" +
          "  });"
      },*/
    ]
  },
  hooks: {
    generate: {
      async done(builder: any) {
        // This makes sure nuxt generate does finish without running into a timeout issue.
        // See https://github.com/nuxt-community/firebase-module/issues/93
        const appModule = await import('./.nuxt/firebase/app.js')
        const { session } = await appModule.default(
          builder.options.firebase.config,
          {
            res: null
          }
        )
        try {
          session.database().goOffline()
        } catch (e) { }
        try {
          session.firestore().terminate()
        } catch (e) { }
      }
    }
  },
  loading: { color: '#ffab00', height: '5px', continuous: true },
  components: true,
  router: {
    //trailingSlash: false,
    //linkActiveClass: 'active',
    linkExactActiveClass: 'exact-active',
    extendRoutes(routes: any, resolve: any) {
      routes.push({
        name: 'custom',
        path: '*',
        component: resolve(__dirname, 'pages/not-found-page.vue')
      })
    },
    //middleware: ['auth']
    middleware: ['Middleware'],
    parseQuery(queryString) {
      return require('qs').parse(queryString);
    },
    stringifyQuery(object) {
      let queryString = require('qs').stringify(object);
      return queryString ? '?' + queryString : '';
    },
  },
  serverMiddleware: [
    bodyParser.json(),
    bodyParser.urlencoded({ extended: true })
    //cookieParser(),
  ],
  transpileDependencies: ['vuex-module-decorators'],
  buildModules: [
    '@nuxt/typescript-build',
    //'@nuxtjs/tailwindcss',
    '@nuxtjs/pwa',
    '@nuxtjs/firebase',
    '@nuxtjs/vuetify'
    //'@nuxtjs/sitemap'
  ],
  vuetify: {
    treeShake: true,
    theme: {
      options: { customProperties: true },
      themes: {
        light: {
          primary: '#0DBDC8',
          secondary: '#ffab00'
        }
      }
    }
  },

  firebase: {
    lazy: false,
    config: {
      apiKey: 'AIzaSyCsY658QMAIgpstw0lFqRSdxZU2UDbPXFY',
      authDomain: 'auth.ofertaup.com',//'e-seller-f54b2.firebaseapp.com',
      databaseURL: 'https://e-seller-f54b2.firebaseio.com',
      projectId: 'e-seller-f54b2',
      storageBucket: 'e-seller-f54b2.appspot.com',
      messagingSenderId: '833909022382',
      appId: '1:833909022382:web:12ec6809a06f68f8e9573f',
      measurementId: 'G-B9FEHQWZYG',
      fcmPublicVapidKey:
        'BJqP3xT_FEz5xgQg2ptuKVPEghVBfxIOlHhASXLVrKBQBj15wntz2GswCQRT_rHpzIg6QQuDMO-INEnI_7f_pc0'
    },
    onFirebaseHosting: false,
    services: {
      auth: {
        persistence: 'local',
        initialize: {
          onAuthStateChangedAction: 'onAuthStateChanged',
          subscribeManually:true
        },
        ssr: true
      },
      firestore: true,
      functions: {
        // emulatorPort: 12345
      },
      storage: true,
      database: true,
      performance: true,
      analytics: true,
      remoteConfig: {
        settings: {
          fetchTimeoutMillis: 60000,
          minimumFetchIntervalMillis: 43200000
        },
        defaultConfig: {
          welcome_message: 'Welcome'
        }
      },
      messaging: {
        createServiceWorker: true,
        actions: [
          {
            action: 'goToLupasGithub',
            url: 'https://github.com/lupas'
          },
          {
            action: 'goToModuleGithub',
            url: 'https://github.com/nuxt-community/firebase-module'
          }
        ]
      }
    }
  },

  modules: [
    '@nuxtjs/axios','nuxt-i18n',
    ['@netsells/nuxt-hotjar', { id: '2367992', sv: '6',}],
    'cookie-universal-nuxt'
  ],
  // plugins: ['~/plugins/lazyMode'],
  plugins: [
    { src: '~/plugins/vue-product-zoomer.js', mode: 'client' },
    { src: '~/plugins/vue-per-slides.js', mode: 'client' },
    { src: '~plugins/vue-video-player.js', mode: 'client' },
    { src: '~plugins/vue-amazon-autocomplete.js', mode: 'client' },
    { src: '~plugins/vue-instantsearch.js', mode: 'client' },
    //{ src:'~/plugins/nuxt-vuelidate.js' },
  ],
  css: [
    'video.js/dist/video-js.css',
    '~assets/style/main.sass'
  ],
  pwa: {
    workbox: {
      importScripts: ['/firebase-auth-sw.js'],
      // by default the workbox module will not install the service worker in dev environment to avoid conflicts with HMR
      // only set this true for testing and remember to always clear your browser cache in development
      dev: process.env.NODE_ENV === 'development'
    },
    //meta:false,
    icon: false,
    meta: {
      lang:"es"
    },
    manifest: {
      lang: 'en',
      display: 'standalone',
    }
  },
  build: {
    //standalone: true,
    /*You can extend webpack config here*/
    transpile: ['vue-instantsearch', 'instantsearch.js/es'],
    extend(config: any, ctx: any) {
    }
  },
  //So important to don't get err query firebase array-contains-any
  render: {
    bundleRenderer: {
      runInNewContext: false
    }
  },
  i18n: {
    /*seo: true,*/
  }
}
