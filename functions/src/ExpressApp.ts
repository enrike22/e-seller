import * as express from "express";
import { Routes } from "./routes/Routes";
import * as cors from 'cors';
import * as bodyParser from "body-parser";

export class ExpressApp {
    public app: express.Application;
    public routePrv: Routes;

    constructor(db:FirebaseFirestore.Firestore, admin:any) {
        this.app = express();
        this.config();
        this.routePrv = new Routes(db, admin)
        this.routePrv.routes(this.app);
        this.app.use('/', this.routePrv.routes);
    }

    private config(){
        this.app.use(bodyParser.json());
        this.app.use(bodyParser.urlencoded({ extended: false }));
        this.app.use(cors({origin: '*'}));
    }
}

//export default new App().app;
