import * as functions from 'firebase-functions';
import * as admin from 'firebase-admin';
import * as moment from "moment";
import "moment-timezone";
moment.locale('es-do');
moment.tz.setDefault("America/Lima");
import { ExpressApp } from "./ExpressApp";
import * as cors from 'cors';

const mercadopago = require ('mercadopago');
mercadopago.configure({
    access_token: 'TEST-3943912916117679-120819-57f5200cfc3f06bebe676d2635451a24-120600374'
    //access_token: 'APP_USR-3943912916117679-120819-5ea8d5ac3068ca67c3e59a147a943761-120600374'
});

//{ memory: functions.VALID_MEMORY_OPTIONS[3], timeoutSeconds: functions.MAX_TIMEOUT_SECONDS } 60 to 540
const runtimeOptions:any = {
    timeoutSeconds: functions.MAX_TIMEOUT_SECONDS,
    memory: functions.VALID_MEMORY_OPTIONS[3]
}


/**
 const accountSid = "ACa165e41303b8914c939d2601ebb73707"//process.env.TWILIO_ACCOUNT_SID;
 const authToken = "0b08c3357e03a12ca986c5ab25f486e6"//process.env.TWILIO_AUTH_TOKEN;
 const client = require('twilio')(accountSid, authToken);
 */

/**Prod*/
admin.initializeApp(functions.config().firebase);
const corsHandler = cors({origin: true});

/**Local*/
/*import * as path from 'path';
const serviceAccount = require(path.join(__dirname, '../e-seller-f54b2-firebase-adminsdk-asw30-b899673c97.json'));
const corsHandler = cors({origin: true});
admin.initializeApp({
    credential: admin.credential.cert(serviceAccount),
    databaseURL: "https://financial-app-4e0c2.firebaseio.com",
    storageBucket: "e-seller-f54b2.appspot.com"
});*/

const db = admin.firestore();
db.settings({ timestampsInSnapshots: true });
//const messaging = admin.messaging()

const expressApp = new ExpressApp(db, admin)

async function update_order(payment_response:any){
    let response = payment_response
    if(response.external_reference){
        //console.log( response.id)
        let res_id = String(response.id)
        let mercadopago_payments = db.collection('method_payments').doc(res_id);
        let mercadopago_py_res:any = await mercadopago_payments.get()
        let mercadopago_model = mercadopago_py_res.data()
        let orders = await db.collection('orders').doc(response.external_reference);
        let orders_models:any = await orders.get()
        let param:any = orders_models.data()
        param.payment["date_approved"] = response.date_approved?response.date_approved:response.date_created
        param.payment["status"] = response.status
        param.payment["method_payment_id"] = response.id
        response["code"] =  param.payment["code"]
        response["title"] =  param.payment["title"]
        if(mercadopago_py_res && mercadopago_model && mercadopago_model.id){
            await mercadopago_payments.update(response)
        }else{
            await mercadopago_payments.set(response)
        }
        await orders.update(param)
        return "OK"
    }else {
        return "Error de datos"
    }
}

async function ipn_notify(topic:any,id:any){
    let payment:any = null
    let merchant_order:any = null
    let paid_amount = 0
    let resp:any = "error"
    switch(topic){
        case "payment":
            payment = await mercadopago.payment.findById(id)
            let response = payment.response
            resp = await update_order(response)
            /*if(response.external_reference){
                console.log( response.id)
                let res_id = String(response.id)
                let mercadopago_payments = db.collection('method_payments').doc(res_id);
                let mercadopago_py_res:any = await mercadopago_payments.get()
                let mercadopago_model = mercadopago_py_res.data()
                let orders = await db.collection('orders').doc(response.external_reference);
                let orders_models:any = await orders.get()
                let param:any = orders_models.data()
                param.payment["date_approved"] = response.date_approved?response.date_approved:response.date_created
                param.payment["status"] = response.status
                param.payment["method_payment_id"] = response.id
                response["code"] =  param.payment["code"]
                response["title"] =  param.payment["title"]
                if(mercadopago_py_res && mercadopago_model && mercadopago_model.id){
                    await mercadopago_payments.update(response)
                }else{
                    await mercadopago_payments.set(response)
                }
                orders.update(param)
                resp = "OK"
            }*/
            //console.log(payment.response.order.id)
            //merchant_order = await mercadopago.merchant_orders.findById(payment.response.order.id)
            break;
        case "merchant_order":
            merchant_order = await mercadopago.merchant_orders.findById(id)
            break;
        case "chargebacks":
            break;
    }

    if(merchant_order){
        merchant_order = merchant_order.response
        merchant_order.payments.forEach((payment:any) => {
            if(payment['status'] == 'approved'){
                paid_amount += payment['transaction_amount'];
            }
        });
        if(paid_amount >= merchant_order.total_amount){
            if (merchant_order.shipments.length>0) { // The merchant_order has shipments
                if(merchant_order.shipments[0].status == "ready_to_ship") {
                    console.log("Totally paid. Print the label and release your item.");
                }
            } else { // The merchant_order don't has any shipments
                console.log("Totally paid. Release your item.");
            }
        } else {
            console.log("Not paid yet. Do not release your item.");
        }
    }
    return resp
}

exports.ipn = functions.https.onRequest(async (request, response) => {
    corsHandler(request, response, async() => {
        let res_ipn = await ipn_notify(request.query.topic,request.query.id)
        response.send({data:res_ipn});
    })
});

exports.refund = functions.https.onRequest(async (request, response) => {
    corsHandler(request, response, async() => {
        if(!request.body.data.payment_id){response.send({data:"Error"}); }
        let payment = await mercadopago.payment.findById(request.body.data.payment_id)
        let payment_res = payment.response
        if (payment_res.status == 'approved'){
            mercadopago.payment.refund(request.body.data.payment_id)
                .then(async (refount_res:any) => {
                    let res_update = await update_order(refount_res.response)
                    response.send({data:res_update});
                })
                .catch((error:any) => {
                    response.send({data:error});
                });
        }
    })
})

exports.cancelled = functions.https.onRequest(async (request, response) => {
    corsHandler(request, response, async() => {
        mercadopago.payment.update({
            id: request.body.data.payment_id,
            status: "cancelled"
        }).then((cancel_res:any) => {
            response.send({data:cancel_res});
        }).catch((error:any)=>{
            response.send({data:error});
        });
    })
})

exports.payment = functions.https.onRequest(async (request, response) => {
    corsHandler(request, response, async () => {
        if (!request.body.data.items && !request.body.data.payer){
            response.status(500).send({ data: "Se requiere datos del producto" });
        }

        let preference = {
            items: request.body.data.items,
            payer:request.body.data.payer,
            back_urls: {
                "success": request.body.data.base_back_urls+"success",
                "failure": request.body.data.base_back_urls+"failure",
                "pending": request.body.data.base_back_urls+"pending"
            },
            auto_return: "approved",
            payment_methods: {
                excluded_payment_types: [
                    {
                        id: "atm"
                    },
                    {
                        id: "ticket"
                    }
                ],
                installments: 1
            },
            shipments:request.body.data.shipments,
            notification_url: "https://us-central1-e-seller-f54b2.cloudfunctions.net/ipn",
            statement_descriptor: "ofertaup.com",
            external_reference: request.body.data.external_reference,
            binary_mode: true,
            //expires: true,
            //expiration_date_from: request.body.data.expiration_date_from,//"2021-01-01T12:00:00.000-04:00",
            //expiration_date_to: request.body.data.expiration_date_to,//"2021-01-28T12:00:00.000-04:00"
        };

        let res = await mercadopago.preferences.create(preference)
        response.send({ data: res });
    })
});

exports.api = functions.runWith(runtimeOptions).https.onRequest(expressApp.app);

/**
 exports.sendWhatsapp = functions.https.onRequest(async (request, response) => {
    corsHandler(request, response, () => {
        //console.log(request.body)
        client.messages
            .create({
                from: 'whatsapp:+14155238886',
                body: 'Hola Maria, tu compra de *Silla gamer* se realizo de forma exitosa, por este medio te informaremos el envio de tu pedido',
                to: 'whatsapp:+51947392421'
            })
            .then((message:any) => {
                console.log(message)
                response.json(message);
            });
        //functions.logger.info("Hello logs!", {structuredData: true});
        //response.json({data:"Hello from Firebase!"});
    });
});*/

/*exports.testFunction = functions.https.onCall(() => {
    console.info('Test Function triggered')
    return { message: "Yeaaahh it's working!" }
})*/

/*exports.sendTestPushMessage = functions.https.onCall(async (data) => {
    // As defined in https://firebase.google.com/docs/reference/fcm/rest/v1/projects.messages
    const image =
        'https://avatars2.githubusercontent.com/u/4020037?s=460&u=c5f9c131d565202d8e530295b130239edd25768d&v=4'
    const message = {
        name: 'testPushMessage',
        data: {},
        notification: {
            title: `Test Push Message`,
            body: 'If you get this, it worked.',
            image,
        },
        android: {},
        webpush: {
            notification: {
                // Adds the image to the push notificationm
                icon: image,
                // Adds actions to the push notification
                actions: [
                    {
                        action: 'goToLupasGithub',
                        title: 'Github: lupas',
                        icon: '',
                    },
                    {
                        action: 'goToModuleGithub',
                        title: 'Firebase Module',
                        icon: '',
                    },
                ],
            },
            fcm_options: {
                // Adds a link to be opened when clicked on the push notification
                link: 'https://nuxt-fire-demo.herokuapp.com/',
            },
        },
        apns: {
            fcm_options: {},
        },
        fcm_options: {},
        token: data.token,
    }
    try {
        // @ts-ignore
        await messaging.send(message)
    } catch (e) {
        console.error(`Did not work to send a message to token ${message.token}`)
        console.error(e)
    }
})*/

/**
 TODO add code agolia
 */
import algoliasearch from "algoliasearch";

const algoliaClient = algoliasearch(
    // functions.config().algolia.appid,
    // functions.config().algolia.apikey
    "EJTBGYWH6R",
    "27f34a68ab08522027448b580ed83b57"
);

const collectionIndexName = "prod_products";
// functions.config().projectId === 'e-seller-f54b2'
//   ? 'prod_products'
//   : 'dev_products'
const collectionIndex = algoliaClient.initIndex(collectionIndexName);

// Create a HTTP request cloud function.
export const sendCollectionToAlgolia = functions.https.onRequest(async (request, response) => {
    corsHandler(request, response, async () => {
        const algoliaRecords: any[] = [];

        const products = db.collection('products');
        const querySnapshot = await products.get();

        querySnapshot.forEach(doc => {
            const document = doc.data();
            // Update:
            let categories_items:any = []
            document.categories.forEach((category: any) => {
                categories_items.push({
                    id: category.id,
                    title: category.title,
                })
            })

            const record = {
                objectID: doc.id,
                id: document.id,
                name: document.name,
                short_name: document.short_name,
                detail_id: document.detail_id,
                description: document.description,
                image: document.image,
                gallery_id: document.gallery_id,
                video: document.video,
                brand: document.brand,
                categories: categories_items,
                price: document.price,
                discount: {
                    amount: document.discount.amount,
                    title: document.discount.title,
                    value: document.discount.value,
                },
                stock: document.stock,
                stock_history: document.stock_history,
                quantity_sold: document.quantity_sold,
                feedback_id: document.feedback_id,
                rating_id: document.rating_id,
                rating_avg: document.rating_avg,
                shipping: {
                    arrival_days: document.arrival_days,
                    business: document.business,
                    image: document.shipping.image,
                    price: document.shipping.price,
                    type: document.shipping.type,
                },
                product_ref: document.product_ref,
                state: document.state,
            };

            algoliaRecords.push(record);
        });

        /*collectionIndex.saveObjects(algoliaRecords, (_error: any, content: any) => {
            response.status(200).send({data:"COLLECTION was indexed to Algolia successfully."});
        });*/

        collectionIndex.saveObjects(algoliaRecords).then(({ objectIDs }) => {
            response.status(200).send(
                {data:objectIDs, sms:"COLLECTION was indexed to Algolia successfully."});
        }).catch(error=>{
            response.status(500).send(
                {data:"error",sms:error});
        });

    })
    }
);

export const collectionOnCreate = functions.firestore
    .document("products/{uid}")
    .onCreate(async (snapshot, context) => {
        await saveDocumentInAlgolia(snapshot);
    });

export const collectionOnUpdate = functions.firestore
    .document("products/{uid}")
    .onUpdate(async (change, context) => {
        await updateDocumentInAlgolia(change);
    });

export const collectionOnDelete = functions.firestore
    .document("products/{uid}")
    .onDelete(async (snapshot, context) => {
        await deleteDocumentFromAlgolia(snapshot);
    });

async function saveDocumentInAlgolia(snapshot: FirebaseFirestore.DocumentSnapshot) {
    if (snapshot.exists) {
        const record = snapshot.data();
        if (record) {
            if (record.state == 1) {
                record.objectID = snapshot.id;
                await collectionIndex.saveObject(record);
            }
        }
    }
}

async function updateDocumentInAlgolia(
    change: functions.Change<FirebaseFirestore.DocumentSnapshot>) {
    const docBeforeChange:any = change.before.data();
    const docAfterChange:any = change.after.data();
    if (docBeforeChange && docAfterChange) {
        await deleteDocumentFromAlgolia(change.after);
        if (docAfterChange.state == 1) {
            await saveDocumentInAlgolia(change.after);
        }
    }
}

async function deleteDocumentFromAlgolia(
    snapshot: FirebaseFirestore.DocumentSnapshot) {
    if (snapshot.exists) {
        const objectID = snapshot.id;
        await collectionIndex.deleteObject(objectID);
    }
}
