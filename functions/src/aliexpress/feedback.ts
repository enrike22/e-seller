import * as fetch from 'node-fetch';
//const fetch = require('node-fetch'); 
const cheerio = require('cheerio');
const faker = require('faker');

const getFeedbackData = (feedbackHtml:any) => {
  const $ = cheerio.load(feedbackHtml);
  const feedbackData:any = [];

  $('.feedback-list-wrap .feedback-item').each((index:any, element:any) => {
    const $elm = $(element);
    let name = $elm
      .find('.user-name')
      .text()
      .trim();

    let country = $elm
      .find('.user-country')
      .text()
      .trim();

    let ratingStyle = $elm.find('.star-view > span').attr('style');

    let rating = ratingStyle.split('width:')[1];
    rating = parseInt(rating) / 20;

    let info:any = {};

    $elm.find('.user-order-info > span').each((index:any, infoKey:any) => {
      const key = $(infoKey)
        .find('strong')
        .text()
        .trim();

      $(infoKey)
        .find('strong')
        .remove();

      info[key] = $(infoKey)
        .text()
        .trim();
    });

    const feedbackContent = $elm
      .find('.buyer-feedback span:first-child')
      .text()
      .trim();

    let feedbackTime = $elm
      .find('.buyer-feedback span:last-child')
      .text()
      .trim();

    feedbackTime = new Date(feedbackTime);

    let photos:any = [];

    $elm.find('.r-photo-list > ul > li').each((index:any, photo:any) => {
      const url = $(photo)
        .find('img')
        .attr('src');
      photos.push(url);
    });

    const data = {
      name: name,
      displayName: faker.name.findName(),
      country: country,
      rating: rating,
      info: info,
      date: feedbackTime,
      content: feedbackContent,
      photos: photos
    };

    feedbackData.push(data);
  });

  return feedbackData;
};

module.exports = {
  get: async function(productId:any, ownerMemberId:any, count:any, limit:any) {
    let allFeedbacks:any = [];
    let totalPages = Math.ceil(count / limit);
    totalPages = limit
    /** If totalPages are greater than 10, i.e. if reviews are > 100, limit it to 100 or 10 pages */
    if (totalPages > 10) {
      totalPages = 10;
    }

    for (let currentPage = 1; currentPage <= totalPages; currentPage++) {
      const opts = {
        headers: {
            cookie: 'xman_us_f=x_locale=es_ES'
        }
    };
      const feedbackUrl = `https://feedback.aliexpress.com/display/productEvaluation.htm?v=2&page=${currentPage}&currentPage=${currentPage}&productId=${productId}&ownerMemberId=${ownerMemberId}`;
      //@ts-ignore
      const feedbackResponse = await fetch(feedbackUrl,opts);
      const feedbackHtml = await feedbackResponse.text();

      const data = getFeedbackData(feedbackHtml);
      allFeedbacks = [...allFeedbacks, ...data];
    }

    return allFeedbacks;
  }
};
