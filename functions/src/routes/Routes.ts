import {Request, Response} from "express"; //NextFunction
//import {TransferCreditoToDevito} from "../controllers/TransferCreditoToDevito";
import {Product} from "../controllers/Product"


export class Routes {

    //public transferCreditoToDevito:TransferCreditoToDevito = new TransferCreditoToDevito()
    public product:Product
    public db: FirebaseFirestore.Firestore;
    public fbAdmin: any

    constructor(db:FirebaseFirestore.Firestore, admin:any){
        this.db = db
        this.fbAdmin = admin
        this.product = new Product(this.db, this.fbAdmin )
    }
    // @ts-ignore
    private methodAsync = fn => (...args:any) => fn(...args).catch((args[2]));
    // @ts-ignore
    private methodAsyncs = fn => (...args:any) => fn(...args).catch((args[2]));

    public routes(app:any): void {
        app.route('/').get((req: Request, res: Response) => {
            res.status(200).send({
                message: 'Welcome to the finalcial api v1'
            })
        })
        // Authentication
        //app.route('/credit-to-debit').post(this.methodAsync(this.transferCreditoToDevito.sendTokenEmail));
        //app.route('/create-product').post(this.methodAsync(this.product.createProduct))
        app.route('/create-product').post(this.methodAsyncs((req: Request, res: Response)=>{
            let product = new Product(this.db,this.fbAdmin)
            return product.createProduct(req, res)
        }))
    }

}
