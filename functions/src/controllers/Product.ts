import {Request, Response} from "express";
import {v4 as uuidv4} from "uuid";
import * as moment from "moment";
import * as admin from 'firebase-admin';

const aliexpress = require('../aliexpress/aliexpressProductScraper');
const { warn } = require("firebase-functions/lib/logger");

export class Product {

    public db: FirebaseFirestore.Firestore;
    public fbAdmin: typeof admin
    constructor(db:FirebaseFirestore.Firestore, fbAdmin:any) {
        this.db = db;
        this.fbAdmin = fbAdmin;
    }

    public async createProduct(req: Request, res: Response) {
        if(req.body && req.body.product_id){
            console.log("Producto-> "+req.body.product_id);
            const product = await aliexpress(req.body.product_id,1);
            //response.json(product)
            let doc_products = this.db.collection('products').doc();
            let doc_details = this.db.collection('details').doc();
            let doc_gallery = this.db.collection('gallery').doc();
            let doc_ratings = this.db.collection('ratings').doc();
            let doc_feedbacks = this.db.collection('feedbacks').doc();

            let param_to_create = {
                product:product,
                id: doc_products.id,
                detail_id: doc_details.id,
                gallery_id: doc_gallery.id,
                rating_id: doc_ratings.id,
                feedback_id: doc_feedbacks.id,
            }
            let fb_product = await this.create_product(param_to_create)
            //await doc_products.set(fb_product)

            let param_to_details = {
                id: doc_details.id,
                specs: product.specs,
                product_id: doc_products.id
            }
            let fb_details = await this.details(param_to_details)
            //await doc_details.set(fb_details)

            let param_to_feedback = {
                id: doc_feedbacks.id,
                feedback: product.feedback,
                product_id: doc_products.id
            }
            let fb_feedback = await this.feedbacks(param_to_feedback)

            //await doc_feedbacks.set(fb_feedback)

            //await this.saveDescription(product)

            let param_to_galery = {
                id: doc_gallery.id,
                product_id: doc_products.id,
                images: product.images
            }
            let fb_galery = await this.galery(param_to_galery)
            //await doc_gallery.set(fb_galery)

            let param_to_ratings = {
                id:doc_ratings.id,
                ratings:product.ratings,
                product_id:doc_products.id
            }
            let fb_ratings = await this.ratings(param_to_ratings)
            //await doc_ratings.set(fb_ratings)

            if (fb_product && fb_details && fb_feedback && product && fb_galery && fb_ratings){
                await doc_products.set(fb_product)
                await doc_details.set(fb_details)
                await doc_feedbacks.set(fb_feedback)
                await doc_gallery.set(fb_galery)
                await doc_ratings.set(fb_ratings)
                await this.saveDescription(product)
                res.json({
                    id: doc_products.id,
                    product_ref: product.productId,
                    detail_id: doc_details.id,
                    gallery_id: doc_gallery.id,
                    rating_id: doc_ratings.id,
                    feedback_id: doc_feedbacks.id,
                });
            }else {
                res.status(500).json({msg:"error, id del producto no identificado"});
            }
        }else {
            res.status(500).json({msg:"error, id del producto no identificado"});
        }

    }


    private async saveDescription(product:any) {
        try {
            const bucket = this.fbAdmin.storage().bucket()
            let desc = product.title.split(",")[0];
            desc = desc.replace(/ /g, '-');
            const file = await bucket.file(`products/description/${desc}.html`);
            const options = {
                resumable: false,
                metadata: {
                    contentType: "text/html; charset=UTF-8",
                    metadata: {firebaseStorageDownloadTokens: uuidv4()}
                }
            }
            await file.save(product.description, options)
            return await file.getSignedUrl({action: 'read', expires: '03-12-2500'})
        }catch (e) {
            warn("Error al guardar la descripcion", {
                key1: e.message,
            });
            return e
        }
    }

    private async details(params:any) {
        let auditoryDay = moment().toDate();

        let detail_list:any = []
        if(params.specs){
            params.specs.map((vl:any)=>{
                detail_list.push({
                    title:vl.attrName,
                    value:vl.attrValue
                })
            })
        }

        let detail_model:any = {
            detail:detail_list,
            id: params.id,
            product_id: params.product_id,
            created_at: this.fbAdmin.firestore.Timestamp.fromDate(auditoryDay),
            update_at: "",
        }

        return detail_model
    }

    private async ratings(params:any) {

        let ratings_model:any = {
            id: params.id,
            product_id: params.product_id,
            total_star: params.ratings.totalStar,
            average_star: params.ratings.averageStar,
            tota_start_count: params.ratings.totalStartCount,
            five_star_count: params.ratings.fiveStarCount,
            four_star_count: params.ratings.fourStarCount,
            three_star_count: params.ratings.threeStarCount,
            two_star_count: params.ratings.twoStarCount,
            one_star_count: params.ratings.oneStarCount
        }

        return ratings_model
    }

    private async galery(params:any) {
        let auditoryDay = moment().toDate();
        let image_list:any = []
        params.images.forEach((vl:any) => {
            image_list.push({
                zoom:vl+"_Q90.jpg",
                normal:vl+"_Q60.jpg", //zise: _300x300.jpg quality: _Q100.jpg
                small:vl+"_300x300.jpg",
            })
        });

        let galery_model:any = {
            id: params.id,
            images: image_list,
            product_id:params.product_id,
            created_at: this.fbAdmin.firestore.Timestamp.fromDate(auditoryDay),
            update_at: "",
        }

        return galery_model
    }

    private async feedbacks(params:any) {
        //let batch = db.batch();
        let auditoryDay = moment().toDate();
        let coment_list:any = []

        await params.feedback.forEach((vl:any) => {
            //let doc = await db.collection('feedbacks').doc();
            let coment = vl.content.replace(/aliexpress/g, '');
            coment_list.push(
                {
                    content: coment,
                    date:vl.date,
                    photos:vl.photos,
                    user_name: vl.displayName,
                    value: vl.rating,
                    user_id: "default",
                }
            )
            //await batch.set(doc, feedbacks_model);
        });

        let feedbacks_model:any = {
            id: params.id,
            coment_list: coment_list,
            product_id: params.product_id,
            created_at: this.fbAdmin.firestore.Timestamp.fromDate(auditoryDay),
            update_at: ""
        }

        return feedbacks_model
        /*await batch.commit().then(() => {
            return "Se agrego:  "+ params.feedback.length+"  objetos"
        }).catch(err=>{
            return err.message
        });*/
    }

    private async create_product(params:any) {
        let product = params.product
        let auditoryDay = moment().toDate();

        let price_mx = product.originalPrice.max;
        let descount = product.salePrice.max;

        let name_brand = product.specs.find((vl:any)=>vl.attrName == "Nombre de la marca")
        name_brand = name_brand ? name_brand.attrValue : ''

        let percent = (price_mx - descount)/price_mx
        let percentage_title = Math.round(percent*100)+"%"
        let percengae_value = Math.round(percent*100)/100

        let desc = product.title.split(",")[0];
        desc = desc.replace(/ /g, '-');

        let video_url = `https://cloud.video.taobao.com/play/u/3434254481/p/1/e/6/t/10301/${product.videoId}.mp4`
        //let video_url = `https://cloud.video.taobao.com/play/u/3434254481/p/1/e/6/t/10301/277987045803.mp4`

        let firebase_product:any = {
            categories:[
                {title:"informática", id:"h8R3rt5i01TVC21VnXjK"}
            ],
            product_ref: product.productId,
            brand: name_brand,
            description: desc+".html",
            id: params.id,
            detail_id: params.detail_id,
            gallery_id: params.gallery_id,
            rating_id: params.rating_id,
            feedback_id: params.feedback_id,
            price: product.originalPrice.max,//775.47
            discount:{
                amount: product.salePrice.max,//575.47
                title: percentage_title,//16%
                value: percengae_value//0.16
            },
            shipping:{
                business:"urbano express",
                image:"https://firebasestorage.googleapis.com/v0/b/e-seller-f54b2.appspot.com/o/icons%2Furbano-express.png?alt=media&token=1a7e2bab-783c-4a06-8f05-bf4d2237ba19",
                arrival_days:22,
                type:"internacional",
                price:0
            },
            image: product.images[0]+"_Q80.jpg",//"_220x220.jpg",
            //max_commission: "0.1",
            name: product.title,
            quantity_sold: product.orders,
            //ranking: product.storeInfo.rating
            rating_avg: product.ratings.averageStar,
            short_name: product.title.split(",")[0],
            state: 1,
            stock: product.totalAvailableQuantity,
            stock_history: "0" ,
            video: video_url,
            created_at: this.fbAdmin.firestore.Timestamp.fromDate(auditoryDay),
            update_at: "",
        }

        return firebase_product
    }

}
