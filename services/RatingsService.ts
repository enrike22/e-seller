import {FIREBASE} from '~/config/HttpBase';

export class RatingsService  {

    async getFeedbacksByProduct(param:any){
        let docRef = FIREBASE.baseFirestore
            .collection('feedbacks')
            .where("product_id", "==",param.id)
        return await docRef.get();
    }

    async getFeedbacksById(param:any) {
        let docRef = FIREBASE.baseFirestore
        .collection('feedbacks').doc(param.id)
        return await docRef.get();
    }

}
