import {FIREBASE} from '~/config/HttpBase';
export class CategoryServices  {

    async getAllCategories(){
        const messageRef = FIREBASE.baseFirestore
            .collection('categories')
        return await messageRef.get()
    }

    async getCategoryByPath(path:any){
        const messageRef = FIREBASE.baseFirestore
            .collection('categories')
        let docRef = messageRef.where("path","==",path)
        return await docRef.get()
    }
}
