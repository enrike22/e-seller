import {FIREBASE} from '~/config/HttpBase';

export class RegionServices  {

    async getDepartment(){
        let docRef = FIREBASE.baseFirestore
            .collection('department')
        return await docRef.get();
    }

    async getDepartmentById(id:any){
        let docRef = FIREBASE.baseFirestore
            .collection('department')
            .where("id","==",id)
        return await docRef.get();
    }

    async getProvince(param:any){
        let docRef = FIREBASE.baseFirestore
            .collection('province')
            .where("department_id","==",param.id)
        return await docRef.get();
    }

    async getDistrict(param:any){
        let docRef = FIREBASE.baseFirestore
            .collection('district')
            .where("province_id","==",param.id)
        return await docRef.get();
    }

}
