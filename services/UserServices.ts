import {FIREBASE} from '~/config/HttpBase';
import moment from 'moment'
import firebase from "firebase/app";
import 'firebase/firestore';
export class UserServices  {

    async setAddress(param:any){
        let docRef = FIREBASE.baseFirestore.collection('addresses').doc()
        let auditoryDay = moment().toDate();
        param['created_at'] = firebase.firestore.Timestamp.fromDate(auditoryDay);
        param['id'] = docRef.id
        return await docRef.set(param).then(()=>{
            return {docId:docRef.id}
        })
    }

    async getAddress(param:any){
        let docRef = FIREBASE.baseFirestore.collection('addresses')
        .where('user_id','==',param.id)
        .where('default','==',true)
        .limit(param.limit)
        return await docRef.get()
    }

    async getAddressAll(param:any){
        let docRef = FIREBASE.baseFirestore.collection('addresses')
        .where('user_id','==',param.id)
        .orderBy('created_at','asc')
        return await docRef.get()
    }

    async createUser(param:any){
        let docRef = FIREBASE.baseFirestore.collection('users').doc(param.user_id)
        let auditoryDay = moment().toDate();
        param['created_at'] = firebase.firestore.Timestamp.fromDate(auditoryDay);
        param['id'] = docRef.id
        return await docRef.set(param).then(()=>{
            return {docId:docRef.id}
        })
    }

    async updatePhoneNumber(param:any){
        let docRef = FIREBASE.baseFirestore.collection('users').doc(param.id);
        let auditoryDay = moment().toDate();
        param['updated_at'] = firebase.firestore.Timestamp.fromDate(auditoryDay);
        return await docRef.update(param)
    }

    async getUserDetail(param:any){
        let docRef = FIREBASE.baseFirestore.collection('users').doc(param.id);
        return await docRef.get()
    }
}
