import {FIREBASE} from '~/config/HttpBase';
import moment from "moment";
import firebase from "firebase/app";
import 'firebase/firestore';
export class OrdersServices  {

    async setCreateOrder(param:any){
        const docRef = FIREBASE.baseFirestore.collection('orders').doc()
        let auditoryDay = moment().toDate();
        param['created_at'] = firebase.firestore.Timestamp.fromDate(auditoryDay);
        param['id'] = docRef.id
        return await docRef.set(param).then(()=>{
            return {docId:docRef.id}
        })
    }

    async updateOrder(param:any){
        const docRef = FIREBASE.baseFirestore.collection('orders').doc(param.id)
        let orderModel = await docRef.get()
        orderModel = await orderModel.data()
        let auditoryDay = moment().toDate();
        orderModel['updated_at'] = firebase.firestore.Timestamp.fromDate(auditoryDay);
        if(param.payment_link){
            orderModel['payment_link'] = param.payment_link
        }else {
            if(orderModel['payment']['status']=='approved'){
                return {docModel:orderModel}
            }else {
                orderModel['state'] = param.state
                orderModel['payment']['status'] = param.payment_status
                orderModel['payment']['date_failure'] = firebase.firestore.Timestamp.fromDate(auditoryDay);
            }
        }
        return await docRef.update(orderModel).then(()=>{
            return {docModel:orderModel}
        })
    }

    async getOrderList(param:any){
        const docRef = FIREBASE.baseFirestore.collection('orders')
        let queryDoc = docRef.where("customer_id", "==",param.id)
            .where("payment.status","==","approved")
            .where("created_at","!=","");
        return await queryDoc.orderBy("created_at", "desc").get()
    }

    async getAllOrderList(){
        const docRef = FIREBASE.baseFirestore.collection('orders')
        let queryDoc = docRef.where("created_at","!=","");
        return await queryDoc.orderBy("created_at", "desc").get()
    }
}
