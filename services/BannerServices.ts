import {FIREBASE} from '~/config/HttpBase';

export class BannerServices  {

    async getAllBanner(){
        let messageRef = FIREBASE.baseFirestore.collection('banners');
        messageRef = messageRef.where('state','==',1).orderBy('position').limit(4);
        return await messageRef.get()
    }
}
