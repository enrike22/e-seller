import {FIREBASE} from '~/config/HttpBase';
import firebase from "firebase/app";
import 'firebase/firestore';
export class ProductService  {

    async getAllDetailProduct(param:any){
        const messageRef = FIREBASE.baseFirestore
            .collection('products')
            .where("id", "==",param.id)
        return await messageRef.get()
    }

    async getGalleryProduct(param:any){
        const messageRef = FIREBASE.baseFirestore
            .collection('gallery')
            .where("product_id", "==",param.id)
        return await messageRef.get()
    }

    async getDetailProduct(param:any){
        const messageRef = FIREBASE.baseFirestore
            .collection('details')
            .where("product_id", "==",param.id)
        return await messageRef.get()
    }

    async getPopularProducts(){
        const messageRef = FIREBASE.baseFirestore
            .collection('products')
            .where('state','==',1)
            .orderBy('quantity_sold','desc')
            .limit(10);
        return await messageRef.get()
    }

    async getDiscountProduct(){
        let messageRef = await FIREBASE.baseFirestore
            .collection('products')
            .where('state','==',1)
            .orderBy('discount.value','desc')
            .limit(10);
        return await messageRef.get()
    }

    async getProductsByCategory(param:any){
        let messageRef = await FIREBASE.baseFirestore.collection('products')
        messageRef = messageRef.where("categories", "array-contains-any",param.categories)
        messageRef = messageRef.where('state','==',1)
        messageRef = messageRef.where("id","!=",param.id).orderBy('id').limit(10);
        return await messageRef.get()
    }

    async getAllProductsCategory(categories:any){
        let messageRef = await FIREBASE.baseFirestore.collection('products')
        messageRef = messageRef.where("categories", "array-contains-any", categories)
            .where('state','==',1).orderBy('id');
        return await messageRef.limit(10).get()
    }

    async getNextProductsCategory(param:any){
        let messageRef = await FIREBASE.baseFirestore.collection('products')
        messageRef = messageRef.where("categories", "array-contains-any", param.categories)
            .where('state','==',1)
            .orderBy('id').startAfter(param.product_id);
        return await messageRef.limit(10).get()
    }

    async getDiscountNuxtProduct(discount_value:any){
        let messageRef = await FIREBASE.baseFirestore
            .collection('products')
            .where('state','==',1)
            .orderBy('discount.value','desc')
            .startAfter(discount_value)
            .limit(10);
        return await messageRef.get()
    }
}
